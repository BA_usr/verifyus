import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.0
import "controls"


Window  {
    id: signWindow
    width: 800
    height: 600
    minimumWidth: 800
    minimumHeight: 600
    maximumWidth: 800
    maximumHeight: 600
    visible: true
    title: qsTr("Sign In Face Look")

//Rectangle {
//    id: signWindow
//    width: 800
//    height: 600
//    visible: true

    property string colorForText: "#242424"
    property string usernameLine: ""
    property string passwordLine: ""

    Row {
        id: rectangle
        anchors.fill: parent

        Image {
            id: image
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            source: "../assets/images/png/03.png"
            anchors.leftMargin: 30
            anchors.bottomMargin: 30
            anchors.topMargin: 30
            fillMode: Image.PreserveAspectFit


        }

        Rectangle {
            id: panelSign
            color: "#ffffff"
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.leftMargin: 450
            anchors.topMargin: 30
            anchors.bottomMargin: 30
            anchors.rightMargin: 30
            radius: 16
            anchors.left: parent.left

            DefaultCustomBtn {
                id: defaultCustomBtn
                y: 446
                height: 45
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 49
                anchors.rightMargin: 70
                anchors.leftMargin: 70
                textBtn: "Войти"
            }

            Text {
                id: icon
                y: 36
                width: 21
                height: 12
                text: qsTr("Sign In")
                anchors.top: parent.top
                font.pixelSize: 14
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 80
                color: colorForText
            }

            Text {
                id: title
                text: qsTr("Введите данные для авротизации")
                anchors.top: parent.top
                font.pixelSize: 12
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 100
                color: colorForText
            }

            Text {
                id: description
                text: "\uE716"
                font.family: "Segoe MDL2 Assets"
                anchors.top: parent.top
                font.pixelSize: 40
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                anchors.topMargin: 24
                anchors.horizontalCenter: parent.horizontalCenter
                color: colorForText
            }

            Rectangle {
                id: block1
                height: 49
                color: "#ffffff"
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.topMargin: 200
                anchors.leftMargin: 50
                anchors.rightMargin: 50
                radius: 15

                Rectangle {
                    id: box1
                    color: "#ffffff"
                    anchors.fill: parent
                    anchors.leftMargin: 5
                    anchors.rightMargin: 5
                    anchors.bottomMargin: 5
                    anchors.topMargin: 5
                    clip: true

                    TextEdit {
                        id: username
                        text: usernameLine
                        anchors.fill: parent
                        font.pixelSize: 12
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                    }
                }
            }

            DropShadow {
                anchors.fill: block1
                horizontalOffset: 0
                verticalOffset: 0
                samples: 10
                color: "#80000000"
                source: block1
                anchors.rightMargin: 0
                anchors.bottomMargin: 2
                anchors.leftMargin: 0
                anchors.topMargin: -2
                z: 0
            }

            Rectangle {
                id: block2
                height: 49
                color: "#ffffff"
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.topMargin: 290
                anchors.leftMargin: 50
                anchors.rightMargin: 50
                radius: 15

                Rectangle {
                    id: box2
                    color: "#ffffff"
                    anchors.fill: parent
                    anchors.leftMargin: 5
                    anchors.rightMargin: 5
                    anchors.bottomMargin: 5
                    anchors.topMargin: 5
                    clip: true

                    TextEdit {
                        id: password
                        text: passwordLine
                        anchors.fill: parent
                        font.pixelSize: 12
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                    }
                }
            }

            DropShadow {
                anchors.fill: block2
                horizontalOffset: 0
                verticalOffset: 0
                samples: 10
                color: "#80000000"
                source: block2
                anchors.rightMargin: 0
                anchors.bottomMargin: 2
                anchors.leftMargin: 0
                anchors.topMargin: -2
                z: 0
            }

            Text {
                id: textReminder
                y: 497
                width: 220
                height: 31
                text: "Если забыли данные для входа, обратитесь к администратору для восстановления"
                color: "#abacad"
                font.pixelSize: 11
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                wrapMode: Text.WordWrap
                anchors.horizontalCenterOffset: 0
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Text {
                id: dataText1
                text: qsTr("Логин")
                anchors.left: parent.left
                anchors.top: parent.top
                font.pixelSize: 12
                anchors.topMargin: 170
                anchors.leftMargin: 50
            }

            Text {
                id: dataText2
                text: qsTr("Пароль")
                anchors.left: parent.left
                anchors.top: parent.top
                font.pixelSize: 12
                anchors.topMargin: 260
                anchors.leftMargin: 50
            }
        }

        DropShadow {
            anchors.fill: panelSign
            horizontalOffset: 0
            verticalOffset: 0
            samples: 10
            color: "#80000000"
            source: panelSign
            anchors.rightMargin: 0
            anchors.bottomMargin: 2
            anchors.leftMargin: 0
            anchors.topMargin: -2
            z: 0
        }
    }
}

