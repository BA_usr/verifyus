import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.0
import "controls"


Window  {
    id: mainWindow
    width: 1200
    height: 800
    minimumWidth: 700
    minimumHeight: 600
    visible: true
    color: "#00000000"
    title: qsTr("Face Look")

    // Remove default title bar

    flags: Qt.Window | Qt.FramelessWindowHint



//Rectangle {
//    id: mainWindow
//    width: 1200
//    height: 800
//    visible: true
//    color: "#00000000"


    // Propertys

    property int windowStatus: 0
    property int windowMargin: 10
    property int windowWidth: 1200
    property int windowHeight: 800

    // Internal functions

    QtObject{
        id: internal

        function resetResizeBoard(){
            resizeLeft.visible = true
            resizeRight.visible = true
            resizeTop.visible = true
            resizeBottom.visible = true

            resizeLowerRightCorner.visible = true
            resizeLowerLeftCorner.visible = true
            resizeTopLeftCorner.visible = true
            resizeTopRightCorner.visible = true
        }

        function maximizedRestore(){
            if (windowStatus == 0){
                mainWindow.showMaximized()
                windowStatus = 1
                windowMargin = 0

                btnMaximizeRestore.btnText = "\uE923"

                resizeLeft.visible = false
                resizeRight.visible = false
                resizeTop.visible = false
                resizeBottom.visible = false

                resizeLowerRightCorner.visible = false
                resizeLowerLeftCorner.visible = false
                resizeTopLeftCorner.visible = false
                resizeTopRightCorner.visible = false

            } else {
                mainWindow.showNormal()
                windowStatus = 0
                windowMargin = 10

                btnMaximizeRestore.btnText = "\uE922"

                internal.resetResizeBoard()
            }
        }

        function ifMaximizedWindowResore(){
            if (windowStatus == 1){
                mainWindow.showNormal()
                windowStatus = 0
                windowMargin = 10

                btnMaximizeRestore.btnText = "\uE922"

                internal.resetResizeBoard()
            }
        }

        function restoreMargins(){
            windowStatus = 0
            windowMargin = 10

            btnMaximizeRestore.btnText = "\uE922"

            internal.resetResizeBoard()
        }

    }

    Rectangle {
        id: bg
        visible: true
        color: "#ffffff"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.rightMargin: windowMargin
        anchors.leftMargin: windowMargin
        anchors.bottomMargin: windowMargin
        anchors.topMargin: windowMargin
        z: 1

        Rectangle {
            id: appContainer
            x: -9
            y: -9
            visible: true
            color: "#ffffff"
            anchors.fill: parent
            anchors.rightMargin: 1
            anchors.leftMargin: 1
            anchors.bottomMargin: 1
            anchors.topMargin: 1

            Rectangle {
                id: topBar
                x: -1
                y: -1
                height: 35
                visible: true
                color: "#e8eaed"
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.rightMargin: 0
                anchors.leftMargin: 0
                anchors.topMargin: 0

                ToggleButton {
                    icon.color: "#000000"
                    font.pointSize: 10
                    onClicked: animationMenu.running = true
                }

                Row {
                    id: rowBtns
                    x: 1032
                    width: 105
                    height: 35
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    anchors.rightMargin: 0

                    TopBarButton{
                        id: btnMinimized
                        btnText: "\ue921"
                        btnColorMouse: "#242424"
                        onClicked: {
                            mainWindow.showMinimized()
                            internal.restoreMargins()
                        }
                    }

                    TopBarButton{
                        id: btnMaximizeRestore
                        btnText: "\uE922"
                        btnColorMouse: "#242424"
                        onClicked: internal.maximizedRestore()
                    }

                    TopBarButton{
                        id: btnClose
                        btnBgMouseOver: "#e81123"
                        btnBgClicked: "#f1707a"
                        btnText: "\ue8bb"
                        onClicked: mainWindow.close()
                    }
                }

                Rectangle {
                    id: navigationBar
                    color: "#e8eaed"
                    anchors.fill: parent
                    anchors.rightMargin: 105
                    anchors.leftMargin: 50

                    DragHandler {
                        onActiveChanged: if(active){
                                             mainWindow.startSystemMove()
                                             internal.ifMaximizedWindowResore()
                                         }
                    }
                }
            }

            Rectangle {
                id: content
                x: -1
                y: 44
                visible: true
                color: "#ffffff"
                anchors.fill: parent
                anchors.topMargin: 35

                Rectangle {
                    id: leftMenu
                    width: 50
                    visible: true
                    color: "#e8eaed"
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: 0
                    anchors.bottomMargin: 0
                    anchors.topMargin: 0
                    enabled: true

                    PropertyAnimation{
                        id: animationMenu
                        target: leftMenu
                        properties: "width"
                        to: if(leftMenu.width == 50) return 200; else return 50
                        duration: 250
                        easing.type: Easing.InOutQuint
                    }

                    Column {
                        id: columnMenus
                        width: 30
                        height: 245
                        anchors.left: parent.left
                        anchors.top: parent.top
                        anchors.leftMargin: 1
                        anchors.topMargin: 13
                        rotation: 0
                        scale: 1
                        transformOrigin: Item.Center
                        spacing: 13

                        TabToggleButton {
                            id: btnViewing
                            width: leftMenu.width
                            btnIconSource: "\uEDE4"
                            text: "Просмотр"
                            font.pointSize: 11
                            onClicked: {
                                btnViewing.isActiveMenu = true
                                btnObservation.isActiveMenu = false
                                btnComparison.isActiveMenu = false
                                btnAddendum.isActiveMenu = false
                                btnChange.isActiveMenu = false

                                btnSettings.isActiveMenu = false

                                btnViewing.btnColorDefault = "#ffffff"
                                btnObservation.btnColorDefault = "#242424"
                                btnComparison.btnColorDefault = "#242424"
                                btnAddendum.btnColorDefault = "#242424"
                                btnChange.btnColorDefault = "#242424"

                                btnSettings.btnColorDefault = "#242424"
                                stackView.replace(Qt.resolvedUrl("pages/viewingPage.qml"))
                            }
                        }

                        TabToggleButton {
                            id: btnObservation
                            width: leftMenu.width
                            btnIconSource: "\uF7EE"
                            text: "Наблюдение"
                            font.pointSize: 11
                            onClicked: {
                                btnViewing.isActiveMenu = false
                                btnObservation.isActiveMenu = true
                                btnComparison.isActiveMenu = false
                                btnAddendum.isActiveMenu = false
                                btnChange.isActiveMenu = false

                                btnSettings.isActiveMenu = false

                                btnViewing.btnColorDefault = "#242424"
                                btnObservation.btnColorDefault = "#ffffff"
                                btnComparison.btnColorDefault = "#242424"
                                btnAddendum.btnColorDefault = "#242424"
                                btnChange.btnColorDefault = "#242424"

                                btnSettings.btnColorDefault = "#242424"
                                stackView.replace(Qt.resolvedUrl("pages/observationPage.qml"))
                            }

                        }

                        TabToggleButton {
                            id: btnComparison
                            width: leftMenu.width
                            btnIconSource: "\uE8B8"
                            text: "Сравнение"
                            font.pointSize: 11
                            onClicked: {
                                btnViewing.isActiveMenu = false
                                btnObservation.isActiveMenu = false
                                btnComparison.isActiveMenu = true
                                btnAddendum.isActiveMenu = false
                                btnChange.isActiveMenu = false

                                btnSettings.isActiveMenu = false

                                btnViewing.btnColorDefault = "#242424"
                                btnObservation.btnColorDefault = "#242424"
                                btnComparison.btnColorDefault = "#ffffff"
                                btnAddendum.btnColorDefault = "#242424"
                                btnChange.btnColorDefault = "#242424"

                                btnSettings.btnColorDefault = "#242424"
                                stackView.replace(Qt.resolvedUrl("pages/comparisonPage.qml"))
                            }

                        }

                        TabToggleButton {
                            id: btnAddendum
                            width: leftMenu.width
                            btnIconSource: "\uE948"
                            text: "Добавление"
                            font.pointSize: 11
                            onClicked: {
                                btnViewing.isActiveMenu = false
                                btnObservation.isActiveMenu = false
                                btnComparison.isActiveMenu = false
                                btnAddendum.isActiveMenu = true
                                btnChange.isActiveMenu = false

                                btnSettings.isActiveMenu = false

                                btnViewing.btnColorDefault = "#242424"
                                btnObservation.btnColorDefault = "#242424"
                                btnComparison.btnColorDefault = "#242424"
                                btnAddendum.btnColorDefault = "#ffffff"
                                btnChange.btnColorDefault = "#242424"

                                btnSettings.btnColorDefault = "#242424"
                                stackView.replace(Qt.resolvedUrl("pages/addendumPage.qml"))
                            }

                        }

                        TabToggleButton {
                            id: btnChange
                            width: leftMenu.width
                            btnIconSource: "\uE70F"
                            text: "Изменение"
                            font.pointSize: 11
                            onClicked: {
                                btnViewing.isActiveMenu = false
                                btnObservation.isActiveMenu = false
                                btnComparison.isActiveMenu = false
                                btnAddendum.isActiveMenu = false
                                btnChange.isActiveMenu = true

                                btnSettings.isActiveMenu = false

                                btnViewing.btnColorDefault = "#242424"
                                btnObservation.btnColorDefault = "#242424"
                                btnComparison.btnColorDefault = "#242424"
                                btnAddendum.btnColorDefault = "#242424"
                                btnChange.btnColorDefault = "#ffffff"

                                btnSettings.btnColorDefault = "#242424"
                                stackView.replace(Qt.resolvedUrl("pages/changePage.qml"))
                            }

                        }

                    }

                    TabToggleButton {
                        id: btnSettings
                        width: leftMenu.width
                        btnIconSource: "\uE713"
                        text: "Настройки"
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 13
                        font.pointSize: 11
                        onClicked: {
                            btnViewing.isActiveMenu = false
                            btnObservation.isActiveMenu = false
                            btnComparison.isActiveMenu = false
                            btnAddendum.isActiveMenu = false
                            btnChange.isActiveMenu = false

                            btnSettings.isActiveMenu = true

                            btnViewing.btnColorDefault = "#242424"
                            btnObservation.btnColorDefault = "#242424"
                            btnComparison.btnColorDefault = "#242424"
                            btnAddendum.btnColorDefault = "#242424"
                            btnChange.btnColorDefault = "#242424"

                            btnSettings.btnColorDefault = "#ffffff"
                            stackView.replace(Qt.resolvedUrl("pages/settingsPage.qml"))
                        }
                    }
                }

                Rectangle {
                    id: contentPages
                    x: 50
                    y: 0
                    visible: true
                    color: "#ffffff"
                    anchors.left: leftMenu.right
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    clip: true
                    focus: false
                    anchors.rightMargin: 0
                    anchors.bottomMargin: 0
                    anchors.leftMargin: 0
                    anchors.topMargin: 0

                    StackView {
                        id: stackView
                        anchors.fill: parent
                        initialItem: {
                            btnViewing.btnColorDefault = "#ffffff"
                            btnViewing.isActiveMenu = true;
                            Qt.resolvedUrl("pages/waitPage.qml");

                        }

                        Component.onCompleted: {
                            stackView.replace(Qt.resolvedUrl("pages/viewingPage.qml"))
                        }
                    }
                }
            }
        }
    }

    DropShadow {
        anchors.fill: bg
        horizontalOffset: 0
        verticalOffset: 0
        samples: 16
        color: "#80000000"
        source: bg
        z: 0
    }

    MouseArea {
        id: resizeLeft
        width: 10
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.leftMargin: 0
        anchors.bottomMargin: 10
        anchors.topMargin: 10
        cursorShape: Qt.SizeHorCursor

        DragHandler{
            target: null
            onActiveChanged: if (active) { mainWindow.startSystemResize(Qt.LeftEdge)}
        }
    }

    MouseArea {
        id: resizeRight
        width: 10
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.rightMargin: 0
        anchors.bottomMargin: 10
        anchors.topMargin: 10
        cursorShape: Qt.SizeHorCursor
        onClicked: animationWindow.running = true

        DragHandler{
            target: null
            onActiveChanged: if (active) { mainWindow.startSystemResize(Qt.RightEdge)}
        }

    }

    MouseArea {
        id: resizeBottom
        height: 10
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        anchors.bottomMargin: 0
        cursorShape: Qt.SizeVerCursor

        DragHandler{
            target: null
            onActiveChanged: if (active) { mainWindow.startSystemResize(Qt.BottomEdge)}
        }
    }

    MouseArea {
        id: resizeTop
        height: 10
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        cursorShape: Qt.SizeVerCursor

        DragHandler{
            target: null
            onActiveChanged: if (active) { mainWindow.startSystemResize(Qt.TopEdge)}
        }
    }

    MouseArea {
        id: resizeLowerRightCorner
        x: 1175
        y: 775
        width: 25
        height: 25
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        cursorShape: Qt.SizeFDiagCursor

        DragHandler{
            target: null
            onActiveChanged: if (active) { mainWindow.startSystemResize(Qt.RightEdge | Qt.BottomEdge)}
        }
    }

    MouseArea {
        id: resizeLowerLeftCorner
        y: 775
        width: 25
        height: 25
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.leftMargin: 0
        anchors.bottomMargin: 0
        cursorShape: Qt.SizeBDiagCursor

        DragHandler{
            target: null
            onActiveChanged: if (active) { mainWindow.startSystemResize(Qt.LeftEdge | Qt.BottomEdge)}
        }
    }

    MouseArea {
        id: resizeTopLeftCorner
        width: 25
        height: 25
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 0
        anchors.topMargin: 0
        cursorShape: Qt.SizeFDiagCursor

        DragHandler{
            target: null
            onActiveChanged: if (active) { mainWindow.startSystemResize(Qt.LeftEdge | Qt.TopEdge)}
        }
    }

    MouseArea {
        id: resizeTopRightCorner
        width: 25
        height: 25
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.rightMargin: 0
        anchors.topMargin: 0
        cursorShape: Qt.SizeBDiagCursor

        DragHandler{
            target: null
            onActiveChanged: if (active) { mainWindow.startSystemResize(Qt.RightEdge | Qt.TopEdge)}
        }
    }
}
