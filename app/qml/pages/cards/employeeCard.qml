import QtQuick 2.15
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.15
import "../../controls"

Item {
    id: item1
    ScrollView {
        id: scrollView
        anchors.fill: parent
        clip: true

        ColumnLayout {
            id: column
            anchors.fill: parent
            spacing: 10
            clip: true

            Rectangle {
                id: rectangle1
                height: 140
                color: "#e9e8eb"
                radius: 10
                Layout.fillWidth: true

                Rectangle {
                    id: column1
                    x: 0
                    width: rectangle1.width * 0.48
                    height: 53
                    color: "#e8eaed"
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.topMargin: 15
                    anchors.leftMargin: 15

                    Text {
                        id: name
                        text: qsTr("Имя")
                        font.pixelSize: 12
                    }

                    Rectangle {
                        id: rectangle2
                        height: 30
                        color: "#ffffff"
                        radius: 5
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.leftMargin: 0
                        anchors.rightMargin: 0
                        anchors.topMargin: 20



                        TextEdit {
                            id: textEdit1
                            x: 0
                            y: 20
                            text: qsTr("")
                            anchors.fill: parent
                            font.pixelSize: 12
                            verticalAlignment: Text.AlignVCenter
                            clip: true
                            anchors.rightMargin: 5
                            anchors.leftMargin: 5
                        }
                    }

                    DropShadow {
                        anchors.fill: rectangle2
                        horizontalOffset: 0
                        verticalOffset: 0
                        samples: 5
                        color: "#80000000"
                        source: rectangle2
                        cached: false
                        z: 0
                    }
                }


                Rectangle {
                    id: column2
                    height: 53
                    color: "#e8eaed"
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.leftMargin: rectangle1.width * 0.53
                    anchors.topMargin: 15
                    anchors.rightMargin: 15
                    Text {
                        id: name1
                        text: qsTr("Фамилия")
                        font.pixelSize: 12
                    }

                    Rectangle {
                        id: rectangle3
                        height: 30
                        color: "#ffffff"
                        radius: 5
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.rightMargin: 0
                        anchors.topMargin: 20
                        anchors.leftMargin: 0

                        TextEdit {
                            id: textEdit2
                            x: 0
                            y: 20
                            text: qsTr("")
                            anchors.fill: parent
                            font.pixelSize: 12
                            verticalAlignment: Text.AlignVCenter
                            clip: true
                            anchors.rightMargin: 5
                            anchors.leftMargin: 5
                            anchors.topMargin: 0
                        }
                    }

                    DropShadow {
                        color: "#80000000"
                        anchors.fill: rectangle3
                        source: rectangle3
                        z: 0
                        horizontalOffset: 0
                        verticalOffset: 0
                        samples: 5
                    }
                }
                Rectangle {
                    id: column3
                    x: 15
                    height: 53
                    color: "#e8eaed"
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.topMargin: 75
                    width: rectangle1.width * 0.48

                    anchors.leftMargin: 15
                    anchors.rightMargin: 450
                    Text {
                        id: name2
                        text: qsTr("Отчество")
                        font.pixelSize: 12
                    }

                    Rectangle {
                        id: rectangle4
                        height: 30
                        color: "#ffffff"
                        radius: 5
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.rightMargin: 0
                        TextEdit {
                            id: textEdit3
                            x: 0
                            y: 20
                            text: qsTr("")
                            anchors.fill: parent
                            font.pixelSize: 12
                            verticalAlignment: Text.AlignVCenter
                            clip: true
                            anchors.rightMargin: 5
                            anchors.leftMargin: 5
                            anchors.topMargin: 0
                        }
                        anchors.topMargin: 20
                        anchors.leftMargin: 0
                    }

                    DropShadow {
                        color: "#80000000"
                        anchors.fill: rectangle4
                        source: rectangle4
                        z: 0
                        horizontalOffset: 0
                        verticalOffset: 0
                        samples: 5
                    }
                }

            }
            Rectangle {
                id: rectangle5
                height: 80
                color: "#e9e8eb"
                radius: 10
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.preferredHeight: 80


                ColumnLayout {
                    id: column4
                    anchors.fill: parent
                    anchors.topMargin: 8
                    anchors.bottomMargin: 8
                    anchors.rightMargin: 15
                    anchors.leftMargin: 15

                    Text {
                        id: text1
                        text: qsTr("Выберите пол")
                        font.pixelSize: 12
                    }

                    ComboBox {
                        id: comboBox
                        height: 30
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.bottom: parent.bottom
                        displayText: "Мужской"
                        textRole: "Женский"
                        anchors.bottomMargin: 3
                        anchors.leftMargin: 0
                    }
                }
            }

            Rectangle {
                id: rectangle6
                height: 80
                color: "#e9e8eb"
                radius: 10

                ColumnLayout {
                    id: column5
                    anchors.fill: parent
                    anchors.rightMargin: 15
                    Text {
                        id: text2
                        text: qsTr("Выберите профессию")
                        font.pixelSize: 12
                    }

                    ComboBox {
                        id: comboBox1
                        height: 30
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 3
                        textRole: "Женский"
                        displayText: "Мужской"
                        anchors.leftMargin: 0
                    }
                    anchors.topMargin: 8
                    anchors.bottomMargin: 8
                    anchors.leftMargin: 15
                }
                Layout.fillWidth: true
                Layout.fillHeight: false
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.preferredHeight: 80
            }

            Rectangle {
                id: rectangle7
                height: 80
                color: "#e9e8eb"
                radius: 10
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.preferredHeight: 80

                ColumnLayout {
                    id: column6
                    anchors.fill: parent
                    anchors.rightMargin: 15
                    anchors.topMargin: 8
                    anchors.bottomMargin: 8
                    anchors.leftMargin: 15

                    Text {
                        id: text3
                        text: qsTr("Укажите дату рождения")
                        font.pixelSize: 12
                    }

                    Rectangle {
                        id: rectangle8
                        height: 30
                        color: "#ffffff"
                        radius: 5
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.rightMargin: 0
                        anchors.topMargin: 20
                        anchors.leftMargin: 0

                        TextField {
                            id: textEdit4
                            text: ("")
                            anchors.fill: parent
                            font.pixelSize: 12
                            verticalAlignment: Text.AlignVCenter
                            anchors.bottomMargin: 0
                            anchors.topMargin: 0
                            rightPadding: 0
                            leftPadding: 0
                            placeholderText: "ГГ-ММ-ДД"
                            anchors.rightMargin: 0
                            clip: true
                            anchors.leftMargin: 10
                            background: Rectangle {
                                border.width: 0
                                color: none
                            }

                        }
                    }

                    DropShadow {
                        anchors.fill: rectangle8
                        horizontalOffset: 0
                        verticalOffset: 0
                        samples: 5
                        color: "#80000000"
                        source: rectangle8
                        cached: false
                        z: 0
                    }
                }
            }

            Rectangle {
                id: rectangle9
                height: 80
                color: "#e9e8eb"
                radius: 10
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.preferredHeight: 80
                ColumnLayout {
                    id: column7
                    anchors.fill: parent
                    anchors.rightMargin: 15
                    anchors.topMargin: 8
                    anchors.bottomMargin: 8
                    anchors.leftMargin: 15
                    Text {
                        id: text4
                        text: qsTr("Почта")
                        font.pixelSize: 12
                    }

                    Rectangle {
                        id: rectangle10
                        height: 30
                        color: "#ffffff"
                        radius: 5
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.rightMargin: 0
                        TextField {
                            id: textEdit5
                            text: ("")
                            anchors.fill: parent
                            font.pixelSize: 12
                            verticalAlignment: Text.AlignVCenter
                            placeholderText: ""
                            anchors.rightMargin: 0
                            anchors.topMargin: 0
                            anchors.bottomMargin: 0
                            clip: true
                            rightPadding: 0
                            anchors.leftMargin: 10
                            background: Rectangle {
                                color: none
                                border.width: 0
                            }
                            leftPadding: 0
                        }
                        anchors.topMargin: 20
                        anchors.leftMargin: 0
                    }

                    DropShadow {
                        anchors.fill: rectangle10
                        horizontalOffset: 0
                        verticalOffset: 0
                        samples: 5
                        color: "#80000000"
                        source: rectangle10
                        cached: false
                        z: 0
                    }


                }
            }

            Rectangle {
                id: rectangle11
                height: 80
                color: "#e9e8eb"
                radius: 10
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.preferredHeight: 80
                ColumnLayout {
                    id: column8
                    anchors.fill: parent
                    anchors.rightMargin: 15
                    anchors.topMargin: 8
                    anchors.bottomMargin: 8
                    anchors.leftMargin: 15
                    Text {
                        id: text5
                        text: qsTr("Номер телефона")
                        font.pixelSize: 12
                    }
                    Rectangle {
                        id: rectangle12
                        height: 30
                        color: "#ffffff"
                        radius: 5
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.top: parent.top
                        anchors.rightMargin: 0
                        TextField {
                            id: textEdit6
                            text: ("")
                            anchors.fill: parent
                            font.pixelSize: 12
                            verticalAlignment: Text.AlignVCenter
                            placeholderText: "Номер"
                            anchors.rightMargin: 0
                            anchors.topMargin: 0
                            anchors.bottomMargin: 0
                            clip: true
                            rightPadding: 0
                            anchors.leftMargin: 10
                            background: Rectangle {
                                color: none
                                border.width: 0
                            }
                            leftPadding: 0
                        }
                        anchors.topMargin: 20
                        anchors.leftMargin: 0
                    }
                    DropShadow {
                        anchors.fill: rectangle12
                        horizontalOffset: 0
                        verticalOffset: 0
                        samples: 5
                        color: "#80000000"
                        source: rectangle12
                        cached: false
                        z: 0
                    }
                }
            }

            Rectangle {
                id: rectangle
                height: 60
                visible: true
                color: "#e9e8eb"
                Layout.fillWidth: true

                DefaultCustomBtn {
                    id: defaultCustomBtn
                    x: 0
                    y: 0
                    width: 99
                    height: 60
                    anchors.right: parent.right
                    anchors.rightMargin: 20
                    textBtn: "Добавить"
                }
            }






        }

    }

}


