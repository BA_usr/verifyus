import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.0
import "../controls"
import QtQuick.Layouts 1.15

Item {
    Rectangle {
        id: rectangle
        color: "#ffffff"
        anchors.fill: parent

        property string visableSwitchBox: 'false'

        //        QtObject{
        //            id: internal

        //            function stackViewtopMargin(){
        //                if (visableSwitchBox === "true"){
        //                    stackView.anchors.topMargin = 80
        //                }
        //                else {
        //                    stackView.anchors.topMargin = 20
        //                }
        //            }
        //        }

        Rectangle {
            id: header
            x: 50
            y: 16
            width: 235
            height: 47
            color: "#e9e8eb"
            radius: 15
            anchors.left: parent.left
            anchors.top: parent.top
            Layout.maximumHeight: 50
            anchors.leftMargin: 50
            anchors.topMargin: 15

            Label {
                id: title
                color: "#242424"
                text: qsTr("Страница добавления")
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pointSize: 16
            }
        }

        DropShadow {
            x: 50
            y: 16
            anchors.fill: header
            horizontalOffset: 0
            verticalOffset: 0
            samples: 5
            color: "#80000000"
            source: header
            anchors.topMargin: 0
            z: 0
        }

        Rectangle {
            id: body
            color: "#e9e8eb"
            radius: 15
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.rightMargin: 50
            anchors.leftMargin: 50
            anchors.bottomMargin: 16
            anchors.topMargin: 76

            Rectangle {
                id: switchBox
                width: 217
                height: 68
                visible: visableSwitchBox
                color: "#e9e8eb"
                anchors.left: parent.left
                anchors.leftMargin: 15
                Layout.leftMargin: 15

                DefaultCustomBtn {
                    id: defaultCustomBtn
                    x: 8
                    y: 33
                    width: 95
                    height: 27
                    textBtn: "Сотрудник"
                }

                DefaultCustomBtn {
                    id: defaultCustomBtn1
                    x: 109
                    y: 33
                    width: 95
                    height: 27
                    textBtn: "Пользователь"
                }

                Text {
                    id: text1
                    x: 8
                    y: 8
                    text: qsTr("Тип добавления данных")
                    font.pixelSize: 14
                }
            }

            StackView {
                id: stackView
                x: 387
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.topMargin: 80
                anchors.rightMargin: 20
                anchors.leftMargin: 20
                anchors.bottomMargin: 20
                initialItem: {
                    Qt.resolvedUrl("cards/employeeCard.qml");

                }
            }


        }

        DropShadow {
            id: dropShadow
            anchors.fill: body
            horizontalOffset: 0
            verticalOffset: 0
            samples: 5
            color: "#80000000"
            source: body
            z: 0
        }
    }
}


