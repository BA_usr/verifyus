import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.0
import "../controls"
import QtQuick.Layouts 1.11

Item {

    Rectangle {
        id: bg
        visible: true
        color: "#ffffff"
        anchors.fill: parent
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0

        ColumnLayout {
            id: columnLayout
            anchors.fill: parent

            Rectangle {
                id: header
                height: 130
                color: "#e9e8eb"
                radius: 15
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.rightMargin: 50
                anchors.leftMargin: 50
                anchors.topMargin: 16

                Rectangle {
                    id: searchLine
                    y: 54
                    height: 58
                    color: "#e8eaed"
                    radius: 5
                    border.width: 0
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.rightMargin: 48
                    anchors.leftMargin: 48

                    Rectangle {
                        id: bgTextEdit
                        color: "#ffffff"
                        radius: 5
                        anchors.fill: parent
                        anchors.leftMargin: 8
                        anchors.bottomMargin: 8
                        anchors.topMargin: 8
                        anchors.rightMargin: 100

                        TextField {
                            id: filterTextField
                            placeholderText: "Enter text here..."
                            anchors.fill: parent
                            font.pixelSize: 12
                            horizontalAlignment: Text.AlignLeft
                            verticalAlignment: Text.AlignVCenter
                            selectionColor: "#0082ff"
                            color: "#242424"
                            padding: 5
                            onTextEdited: {
                                    var filterText = filterTextField.text
                                    TableModel.setFilterString(filterText)
                            }
                        }                     
                    }

                    DefaultCustomBtn {
                        id: defaultCustomBtn
                        x: 425
                        y: 172
                        width: 86
                        height: 42
                        textBtn: "Search"
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        anchors.verticalCenterOffset: 0
                        anchors.rightMargin: 8
                    }
                }

                DropShadow {
                    anchors.fill: searchLine
                    horizontalOffset: 0
                    verticalOffset: 0
                    samples: 10
                    color: "#80000000"
                    source: searchLine
                    anchors.rightMargin: 0
                    anchors.bottomMargin: 2
                    anchors.leftMargin: 0
                    anchors.topMargin: -2
                    z: 0
                }

                Label {
                    id: label
                    x: 48
                    y: 11
                    height: 28
                    color: "#242424"
                    text: qsTr("Страница поиска")
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    anchors.rightMargin: 870
                    anchors.leftMargin: 56
                    font.pointSize: 16
                }
            }

            DropShadow {
                anchors.fill: header
                horizontalOffset: 0
                verticalOffset: 0
                samples: 5
                color: "#80000000"
                source: header
                z: 0
            }
        }

        Rectangle {
            id: body
            color: "#e9e8eb"
            radius: 15
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.rightMargin: 50
            anchors.leftMargin: 50
            anchors.bottomMargin: 16
            anchors.topMargin: 162
            Rectangle {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.rightMargin: 10
                anchors.leftMargin: 10
                anchors.bottomMargin: 10
                anchors.topMargin: 10
                color: "#e9e8eb"

                TableView {
                    id: tableView
                    columnWidthProvider: function (column) { return 160; }
                    rowHeightProvider: function (column) { return 60; }
                    anchors.fill: parent
                    clip: true
                    leftMargin: rowsHeader.implicitWidth
                    topMargin: columnsHeader.implicitHeight
                    model: TableModel
                    width: 350
                    delegate: Rectangle {
                        id: rectangle
                        implicitWidth: 100
                        implicitHeight: 50
                        color: "#e9e8eb"

                        TextEdit {
                            id: items
                            text: display
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                        }

                        Rectangle {
                            id: lineDemarcation
                            height: 1
                            anchors.left: parent.left
                            anchors.right: parent.right
                            anchors.rightMargin: -1
                            anchors.leftMargin: 0
                        }

                        DropShadow {
                            anchors.fill: lineDemarcation
                            horizontalOffset: 0
                            verticalOffset: 0
                            samples: 5
                            color: "#80000000"
                            source: lineDemarcation
                            z: 0
                        }
                    }

                    Row {
                        id: columnsHeader
                        y: tableView.contentY
                        z: 2
                        Repeater {
                            model: tableView.columns > 0 ? tableView.columns : 1
                            Label {
                                width: tableView.columnWidthProvider(modelData)
                                height: 35
                                text: TableModel.headerData(modelData, Qt.Horizontal)
                                color: '#242424'
                                font.pixelSize: 15
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                padding: 10
                                background: Rectangle {
                                    color: "#e9e8eb"
                                }
                            }
                        }
                    }

                    Column {
                        id: rowsHeader
                        x: tableView.contentX
                        z: 2
                        Repeater {
                            model: tableView.rows > 0 ? tableView.rows : 1
                            Label {
                                id: label1
                                width: 35
                                height: tableView.rowHeightProvider(modelData)
                                text: TableModel.headerData(modelData, Qt.Vertical)
                                color: '#aaaaaa'
                                font.pixelSize: 15
                                padding: 10
                                verticalAlignment: Text.AlignVCenter
                                background: Rectangle {
                                    color: "#e9e8eb"
                                }
                            }
                        }
                    }

                    ScrollIndicator.horizontal: ScrollIndicator { }
                    ScrollIndicator.vertical: ScrollIndicator { }
                }
            }
        }

        DropShadow {
            anchors.fill: body
            horizontalOffset: 0
            verticalOffset: 0
            samples: 5
            color: "#80000000"
            source: body
            z: 0
        }
    }

    Item {
        id: __materialLibrary__
    }
}
