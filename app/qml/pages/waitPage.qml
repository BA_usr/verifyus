import QtQuick 2.15

Item {
    Rectangle {
        id: rectangle

        color: "#ffffff"
        anchors.fill: parent

        Text {
            id: text1
            color: "#242424"
            text: qsTr("Пожалуйста ожидайте время загрузки..")
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 14
            horizontalAlignment: Text.AlignHCenter
            font.bold: false
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }

}
