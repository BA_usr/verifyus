import QtQuick 2.15
import QtQuick.Controls 2.15
//import Qt5Compat.GraphicalEffects


Button {
    id: btnToggle
    width: 50
    height: 35

    background: Rectangle{
        id: bgBtn
        color: "#e8eaed"

        Label {
           color: "#242424"
           text: "\ue700"
           anchors.fill: parent
           horizontalAlignment: Text.AlignHCenter
           verticalAlignment: Text.AlignVCenter
           font.family: "Segoe MDL2 Assets"
           antialiasing: true
        }

    }
}
