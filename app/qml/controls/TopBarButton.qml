import QtQuick 2.15
import QtQuick.Controls 2.15

Button {
    id: btnTopBar
    width: 35
    height: 35
    text: ""

    property string btnText: "\uE923"
    property color btnBgDefault: "#e8eaed"
    property color btnBgMouseOver: "#efefef"
    property color btnBgClicked: "#d3d9e5"

    property color btnColorDefault: "#242424"
    property color btnColorMouse: "#ffffff"

    QtObject{
        id: internal

        // MOUSE OVER AND CLICK CHANGE COLOR
        property var dynamicBg: if(btnTopBar.down){
                                       btnTopBar.down ? btnBgClicked : btnBgDefault
                                   } else {
                                       btnTopBar.hovered ? btnBgMouseOver : btnBgDefault
                                   }
        property var dynamicColor: if(btnTopBar.down){
                                       btnTopBar.down ? btnColorMouse : btnColorDefault
                                   } else {
                                       btnTopBar.hovered ? btnColorMouse : btnColorDefault
                                   }

    }

    background: Rectangle{
        id: bgBtn
        color: internal.dynamicBg

        Label {
           color: internal.dynamicColor
           anchors.fill: parent
           horizontalAlignment: Text.AlignHCenter
           verticalAlignment: Text.AlignVCenter
           font.family: "Segoe MDL2 Assets"
           text: btnText
        }

    }
}
