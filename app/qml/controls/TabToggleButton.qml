import QtQuick 2.15
import QtQuick.Controls 2.15

Button{
    id: btnLeftMenu
    text: qsTr("Left Menu Text")

    // CUSTOM PROPERTIES
    property string btnIconSource: ""
    property color btnBgDefault: "#e8eaed"
    property color btnBgMouseOver: "#0082ff"

    property color btnColorDefault: "#242424"
    property color btnColorMouse: "#ffffff"
    property int iconWidth: 28
    property int iconHeight: 28
    property bool isActiveMenu: false

    QtObject{
        id: internal

        // MOUSE OVER AND CLICK CHANGE COLOR/*
        property var dynamicBg: if(btnLeftMenu.down){
                                        btnLeftMenu.down ? btnBgMouseOver : "#00000000"
                                   } else {
                                        btnLeftMenu.hovered ? btnBgMouseOver : "#00000000"
                                   }

        property var dynamicColor: if(btnLeftMenu.down){
                                        btnLeftMenu.down ? btnColorMouse : btnColorDefault
                                   } else {
                                        btnLeftMenu.hovered ? btnColorMouse : btnColorDefault
                                   }
    }

    implicitWidth: 250
    implicitHeight: 35

    background: Rectangle{
        id: bgBtn
        color: btnBgDefault
    }

    contentItem: Item{
        anchors.fill: parent
        id: content
        Rectangle {
            id: btnChanged
            x: 10
            y: 4
            color: btnBgMouseOver
            radius: 5
            visible: isActiveMenu
            width: iconWidth
            height: iconHeight
        }

        Label {
            id: iconBtn
            width: iconWidth
            height: iconHeight
            text: btnIconSource
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            anchors.leftMargin: 10
            visible: true
            antialiasing: true
            color: internal.dynamicColor
            font.family: "Segoe MDL2 Assets"

            background: Rectangle{
                id: bgBtnIcon
                color: internal.dynamicBg
                radius: 5

                Behavior on color {
                    ColorAnimation {
                        duration: 350
                    }
                }
            }

            Behavior on color {
                ColorAnimation {
                    duration: 400
                }
            }

        }

        Text{
            color: "#242424"
            text: btnLeftMenu.text
            font: btnLeftMenu.font
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 70
        }
    }
}

