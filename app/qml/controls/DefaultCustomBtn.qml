import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

Item{

    // CUSTOM PROPERTIES

    // property string btnIconSource: ""
    property color btnBgDefault: "#cccccc"
    property color btnBgMouseOver: "#0082ff"

    property color btnColorDefault: "#242424"
    property color btnColorMouse: "#ffffff"

    property int rds: 8
    property string textBtn: ""

    QtObject{
        id: internal

        // MOUSE OVER AND CLICK CHANGE COLOR/*
        property var dynamicBg: if(customDefaultBtn.down){
                                        customDefaultBtn.down ? btnBgMouseOver : btnBgDefault
                                   } else {
                                        customDefaultBtn.hovered ? btnBgMouseOver : btnBgDefault
                                   }

        property var dynamicColor: if(customDefaultBtn.down){
                                        customDefaultBtn.down ? btnColorMouse : btnColorDefault
                                   } else {
                                        customDefaultBtn.hovered ? btnColorMouse : btnColorDefault
                                   }
    }

    Button {
        id: customDefaultBtn
        text: textBtn
        anchors.fill: parent
        background: Rectangle{
            id: bgBtn
            color: internal.dynamicBg
            radius: rds

        }

    }

}
