import mysql.connector
from mysql.connector import Error

class ConnectToMySQL():
        def __init__(self):
                self.con = None

        def connect(self):
                try:
                        self.con = mysql.connector.MySQLConnection(
                                        host = '127.0.0.1',
                                        user = 'root',
                                        charset='utf8',
                                        password = '',
                                        db = '19261_face_look',
                        )
                except Error as e:
                        print(e)

                return self.con


def outputDate( sql):
        bd = ConnectToMySQL()
        bd.connect()
        con = bd.con
        cursor = con.cursor(dictionary=True)
        cursor.execute(sql)
        result = cursor.fetchall()
        return result
