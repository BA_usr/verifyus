# This Python file uses the following encoding: utf-8
import sys

from PyQt5.QtGui import QGuiApplication
from PyQt5.QtQml import QQmlApplicationEngine

from config.pages.viewingPageConfig import viewingPage
from lib import customTableModel

if __name__ == '__main__':
    app = QGuiApplication(sys.argv)

    engine = QQmlApplicationEngine()
    engine.quit.connect(app.quit)
    engine.load('qml/app.qml')

    model = customTableModel.TableModel(viewingPage.loadDataEmploee(), ["Идентификатор", "Фамилия", "Имя", "Отчество", "Пол", "Специальность", "Дата рождения", "Email", "Номер телефона"])
    proxy_model = customTableModel.MyFilterProxyModel()
    proxy_model.setSourceModel(model)

    engine.rootContext().setContextProperty("TableModel", proxy_model)

    sys.exit(app.exec())
