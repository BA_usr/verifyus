# This Python file uses the following encoding: utf-8
from lib.databaseOperations import outputDate

class viewingPage:
    def __init__(self):
        pass

    def loadDataEmploee():
        resultFromEmployee_data = outputDate("SELECT * FROM `employee`")
        bdEmployee = []
        for i in resultFromEmployee_data:
            employee = ([i['id'], i['first_name'], i['last_name'], i['surname'], i['gender'], i['Position_id'], str(i['dob']), i['email'], i['phone_number']])
            bdEmployee.append(employee)

        return bdEmployee
